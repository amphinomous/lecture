package test.homework9_10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.homework9_10.FindDuplicatesInStream1;

import java.util.Arrays;
import java.util.List;


/**
   * Тест для посика дубликатов в Stream
   * Создает два списка в 1 есть дубликаты во втором только дубликаты из первого
   * проверяем через containsall
 */

class FindDuplicatesInStream1Test {
    List<String> listnames = Arrays.asList("Alice", "Bob", "Alice", "Carl", "Don", "Eugene", "Carl");
    List<String> duplicates = Arrays.asList("Alice", "Carl");


    @Test
    void findDuplicates() {
         {
            Assertions.assertTrue(duplicates.containsAll(FindDuplicatesInStream1.findDuplicates1(listnames)));
            Assertions.assertTrue(duplicates.containsAll(FindDuplicatesInStream1.findDuplicates2(listnames)));
            Assertions.assertTrue(duplicates.containsAll(FindDuplicatesInStream1.findDuplicates3(listnames)));

        }

    }

}