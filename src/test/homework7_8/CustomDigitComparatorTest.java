package test.homework7_8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.homework7_8.CustomDigitComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CustomDigitComparatorTest {
    /**
     // Создаем 2 списка
     // Добавляем значения
     // Сравниваем с результатом который должен получиться. Cначала чет потом не чет
     */

    @Test
    public void compareTest() {

        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();

        list1.add(0);
        list1.add(0);
        list1.add(2);
        list1.add(2);
        list1.add(1);
        list1.add(1);

        list2.add(1);
        list2.add(0);
        list2.add(0);
        list2.add(2);
        list2.add(2);
        list2.add(1);


        Comparator<Integer> comparable = new CustomDigitComparator();
        list2.sort(comparable);

        System.out.println(list1);
        System.out.println(list2);

        Assertions.assertEquals(list1.toString(), list2.toString());
    }
}
