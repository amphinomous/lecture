package test.homework7_8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.homework7_8.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class PersonTest {
    /**
     * Проверка результата выполнения метода {@link Person}
     */
    @Test
    public void compareTo_Test() {

        List<Person> peoplelist = new ArrayList<>();

        peoplelist.add(new Person("Alice", "Osaka", 2));
        peoplelist.add(new Person("Bob", "Osaka", 2));
        peoplelist.add(new Person("Alice", "Rome", 1));
        peoplelist.add(new Person("Carl", "London", 14));
        peoplelist.add(new Person("Don", "Delhi", 41));
        peoplelist.add(new Person("Eugene", "Delhi", 14));
        peoplelist.add(new Person("Carl", "Delhi", 12));
        peoplelist.add(new Person("Carl", "Delhi", 12));


        System.out.println("------before sort--------");
        for (Person u : peoplelist) {
            System.out.println(u);
        }
        System.out.println("-------after sort-----");
        Collections.sort(peoplelist);
        for (Person u : peoplelist) {
            System.out.println(u);

        }
        peoplelist.sort(Person::compareTo);

        Person person = peoplelist.get(2);

        Assertions.assertEquals("Eugene", person.getName());
    }




    /**
     * Проверка конструктора класса {@link Person}
     */
    @Test
    public void Person_Test() {

        Person person = new Person("Carl", "London", 40);

        Assertions.assertEquals("Carl", person.getName());
    }
}
