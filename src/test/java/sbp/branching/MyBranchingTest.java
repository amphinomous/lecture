package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {
    /**
     *      Если utilFunc2 выдает true то возвращается true Проверяем через assertTrue
     */
    @Test

    public void ifElseExample_Success_Test(){
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();

        Assertions.assertTrue(myBranching.ifElseExample());
    }

    /**
     * Если utilFunc2 выдает false то возвращается false Проверяем через assertFalse
     */
    @Test

    public void ifElseExample_Fail_Test(){
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();

        Assertions.assertFalse(result);
    }

    /**
     *     Если utilFunc2 выдает false ожидаем на выходе большее из параметров.
     *     Проверяем сравнением с результатом Max.int
     */
    @Test
    public void maxInt_Success_Test() {
        int i1 = -12;
        int i2 = 6;
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsForTest);
        Assertions.assertEquals(Math.max(i1, i2), myBranching.maxInt(i1, i2));
        System.out.println(myBranching.maxInt(i1, i2));
    }

    /**
     *     Если utilFunc2 выдает true ожидаем на выходе 0.
     *     Проверяем с помощью assertEquals результат и 0
     */
    @Test

    public void maxInt_Fail_Test() {
        int i1 = -12;
        int i2 = 6;
        Utils utilsMaxInt = Mockito.mock(Utils.class);
        Mockito.when(utilsMaxInt.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMaxInt);

        int result = myBranching.maxInt(i1, i2);

        Assertions.assertEquals(0, myBranching.maxInt(i1, i2));
        System.out.println(result + " done");
    }

    /**
     *      при @param = 0 выполняется блок default.
     *      если utilFunc2 false
     *      если utilFunc1 true
     *      Ожидаем вызов utilFunc2 один раз и utilFunc1 0 раз
     */
    @Test

    public void switchExample_0_Test() {

        final int i = 0;

        Utils utilsSwitchTest = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchTest);
        Mockito.when(utilsSwitchTest.utilFunc2()).thenReturn(false);
        Mockito.when(utilsSwitchTest.utilFunc1(anyString())).thenReturn(true);
        myBranching.switchExample(i);


        Mockito.verify(utilsSwitchTest, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc2();

    }

    /**
     *      при @param = 0 выполняется блок default.
     *      если utilFunc2 true
     *      если utilFunc1 true
     *      Ожидаем вызов utilFunc2 один раз и utilFunc1 один раз
     */
    @Test


    public void switchExample_01_Test() {

        final int i = 0;

        Utils utilsSwitchTest = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchTest);
        Mockito.when(utilsSwitchTest.utilFunc2()).thenReturn(true);
        Mockito.when(utilsSwitchTest.utilFunc1(anyString())).thenReturn(true);
        myBranching.switchExample(i);


        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc2();

    }

    /**
     *      при @param = 1 выполняется блок 1.
     *      Ожидаем вызов utilFunc1 затем utilFunc2
     */

    @Test

    public void switchExample_1_Test() {

        final int i = 1;

        Utils utilsSwitchTest = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchTest);

        myBranching.switchExample(i);


        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc2();

    }

    /**
     *      при @param = 2 выполняется блок 2.
     *      Ожидаем вызов только utilFunc2
     */
    @Test

    public void switchExample_2_Test() {
        final int i = 2;
        Utils utilsSwitchTest = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchTest);
        myBranching.switchExample(i);

        Mockito.verify(utilsSwitchTest, Mockito.never()).utilFunc1(anyString());
        Mockito.verify(utilsSwitchTest, Mockito.times(1)).utilFunc2();

    }



}
