package sbp.collections;
import java.util.Collection;

/**
 * Simple array. Can store any objects.
 */

public abstract class CustomArrayImpl<T> implements CustomArray<T>{

    private int capacity = 10;
    private Object[] array;
    private int size = 0;

    /**
     * Constructor standard
     */

    public CustomArrayImpl() {
        array = new Object[capacity];
    }

    /**
     * Constructor with capacity
     */

    public CustomArrayImpl(int capacity) {
        this.capacity = capacity;
        array = new Object[capacity];
    }

    /**
     * Constructor with collection
     */

    public CustomArrayImpl(Collection<T> c) {
        size = c.size();
        ensureCapacity(size);
        Object[] buf = c.toArray();
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, size);
    }

    /**
     * Return size of Array
     */

    @Override
    public int size() {
        return size;
    }

    /**
     * Return true if Array is empty else return false
     */

    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    /**
     * Add single item.
     */

    @Override
    public boolean add(T item) {
        ensureCapacity(size);
        array[size] = item;
        size++;
        return true;
    }

    /**
     * Add all items.
     *
     * @throws IllegalArgumentException if parameter items is null
     */

    @Override
    public boolean addAll(T[] items) {
        for (T item : items) {
            if (item == null) {
                throw new IllegalArgumentException("null!!!");
            }
        }

        ensureCapacity(items.length + size);
        System.arraycopy(items, 0, array, size, items.length);
        size = size + items.length;
        return true;
    }
    /**
     * Add all items.
     *
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(Collection<T> items) {
        for (int i = 0; i < items.size(); i++) {
            if (items.contains(null)) {
                throw new IllegalArgumentException("null!!!");
            }
        }

        ensureCapacity(size + items.size());
        Object[] buf = items.toArray();
        System.arraycopy(buf, 0, array, size, items.size());
        size += buf.length;
        return true;
    }

    /**
     * Add items to current place in array.
     *
     * @param index - index
     * @param items - items for insert
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     * @throws IllegalArgumentException       if parameter items is null
     */

    @Override
    public boolean addAll(int index, T[] items) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (T item : items) {
            if (item == null) {
                throw new IllegalArgumentException("null!!!");
            }
        }

        ensureCapacity(items.length + size);
        Object[] buf = array.clone();
        size += items.length;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, index);
        System.arraycopy(items, 0, array, index, items.length);
        System.arraycopy(buf, index, array, items.length + index, (size - index - items.length));
        return true;
    }

    /**
     * Get item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */

    @Override
    public T get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) array[index];
    }
    /**
     * Set item by index.
     *
     * @param index - index
     * @return old value
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */

    @Override
    public T set(int index, T item) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) (array[index] = item);
    }

    /**
     * Remove item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */

    @Override
    public void remove(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        while (index < array.length - 1) {
            array[index] = array[++index];
        }
        Object[] buf = array.clone();
        size--;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, (buf.length - 1));
    }

    /**
     * Remove item by value. Remove first item occurrence.
     *
     * @param item - item
     * @return true if item was removed
     */

    @Override
    public boolean remove(T item) {
        int index = indexOf(item);
        if (index == -1) {
            return false;
        }
        while (index < array.length - 1) {
            array[index] = array[++index];
        }
        Object[] buf = array.clone();
        size--;
        array = new Object[capacity];
        System.arraycopy(buf, 0, array, 0, (buf.length - 1));
        return true;
    }
    /**
     * Checks if item exists.
     *
     * @param item - item
     * @return true or false
     */

    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(item)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Index of item.
     *
     * @param item - item
     * @return index of element or -1 of list doesn't contain element
     */

    @Override
    public int indexOf(T item) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }
    /**
     * Grow current capacity to store new elements if needed.
     *
     * @param newElementsCount - new elements count
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount >= getCapacity() - 1) {
            Object[] buf = array.clone();
            capacity =(int) (capacity + newElementsCount * 1.5);
            this.array = new Object[capacity];
            System.arraycopy(buf, 0, array, 0, buf.length);
        }
    }
    /**
     * Get current capacity.
     */

    @Override
    public int getCapacity() {
        return capacity;
    }

    /**
     * Reverse list.
     */

    @Override
    public void reverse() {
        for (int i = 0; i < size / 2; i++) {
            Object temp = array[i];
            array[i] = array[size - 1 - i];
            array[size - 1 - i] = temp;
        }
    }

    /**
     * Get copy of current array.
     */

    @Override
    public Object[] toArray() {
        return array;
    }


    /**
     * return array in console
     */

    @Override
    public String toString() {
        String info = "";
        int i = 0;
        while (i < size) {
            info = info + array[i] + " ";
            i++;
        }
        return "[ " + info + "]";
    }
}
