package sbp.homework11_12;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;

public class MyIOExampleNIO2
{
    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException
    {
        File file = new File(fileName);

        if (!file.exists()) return false;

        System.out.println("Абсолютный путь: " + file.getAbsolutePath());
        System.out.println("Относительный путь: " + file.getCanonicalPath());

        if (file.isDirectory()) return false;

        System.out.println("Размер файла: " + file.length() + " байт.");
        System.out.println("Время последнего изменения: " + new Date(file.lastModified()));

        return true;
    }

    /**
     * Создается объект класса {@link Path},
     * проверяется существование и чем является (фалй или директория).
     * Если сущность существует, то выводит в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то выводит в консоль:
     *      - размер
     *      - время последнего изменения
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     * @throws IOException
     */
    public boolean workWithFileNIO(String fileName) throws IOException
    {
        if (fileName.isEmpty()) return false;

        Path path = Paths.get(fileName);

        if (!Files.exists(path)) return false;

        System.out.println("Абсолютный путь: " + path.toAbsolutePath());
        System.out.println("Относительный путь: " + path.getRoot().relativize(path));

        if (Files.isDirectory(path)) return false;

        System.out.println("Размер файла: " + Files.size(path) + " байт.");
        System.out.println("Время последнего изменения: " + Files.getLastModifiedTime(path));

        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileInputStream} и {@link FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException
    {
        File file = new File(sourceFileName);

        if (!file.exists() || file.isDirectory() || !file.exists()) return false;

        InputStream is = null;
        OutputStream os = null;

        try
        {
            is = new FileInputStream(sourceFileName);
            os = new FileOutputStream(destinationFileName);
            byte buffer[] = new byte[1024];

            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
            return false;
        }
        finally {
            if (Objects.nonNull(is) | Objects.nonNull(os)) {
                try {
                    is.close();
                    os.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace(System.out);
                }
            }
        }

        return true;
    }

    /**
     * Метод создает копию файла
     * используя {@link Files}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     * @throws IOException
     */
    public boolean copyFileNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        Files.copy(Paths.get(sourceFileName), Paths.get(destinationFileName));

        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link BufferedInputStream} и {@link BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        File file = new File(sourceFileName);

        if (!file.exists() || file.isDirectory() || !file.exists()) return false;

        InputStream is = null;
        OutputStream os = null;

        try
        {
            is = new BufferedInputStream(new FileInputStream(file));
            os = new BufferedOutputStream(new FileOutputStream(destinationFileName));

            int charCode;
            while ((charCode = is.read()) != -1)
            {
                os.write(charCode);
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
            return false;
        }
        finally {
            if (Objects.nonNull(is) | Objects.nonNull(os)) {
                try {
                    is.close();
                    os.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace(System.out);
                }
            }
        }

        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileReader} и {@link FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        File file = new File(sourceFileName);

        if (!file.exists() || file.isDirectory() || !file.exists()) return false;

        Reader reader = null;
        Writer writer = null;

        try
        {
            reader = new FileReader(file);
            writer = new FileWriter(destinationFileName);

            int charCode;
            while ((charCode = reader.read()) != -1)
            {
                writer.write(charCode);
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
            return false;
        }
        finally {
            if (Objects.nonNull(reader) | Objects.nonNull(writer)) {
                try {
                    reader.close();
                    writer.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace(System.out);
                }
            }
        }

        return true;
    }

    /**
     * Метод создает копию файда
     * используя {@link Files}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithInputOutputStreamNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        try (InputStream in = Files.newInputStream(Paths.get(sourceFileName));
             OutputStream out = Files.newOutputStream(Paths.get(destinationFileName))) {

            int character;
            while ((character = in.read()) != -1)
            {
                out.write(character);
            }

            return true;
        }
    }

    /**
     * Метод создает копию файла
     * используя {@link BufferedReader} и {@link BufferedWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileBufferedNIO(String sourceFileName, String destinationFileName) throws IOException
    {
        if (sourceFileName.isEmpty()) return false;

        if (!Files.exists(Paths.get(sourceFileName))) return false;
        if (Files.isDirectory(Paths.get(sourceFileName))) return false;
        if (Files.exists(Paths.get(destinationFileName))) throw new IOException("File " + destinationFileName + " already exist!");

        try (BufferedReader reader = new BufferedReader(Files.newBufferedReader(Paths.get(sourceFileName)));
             BufferedWriter writer = new BufferedWriter(Files.newBufferedWriter(Paths.get(destinationFileName))))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                writer.write(line);
            }

            return true;
        }
    }
}
