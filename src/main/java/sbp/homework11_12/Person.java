package sbp.homework11_12;



public class Person implements Comparable<Person> {

    /**
     * Реализовать класс Person{name, city, age}, определить метод toString.
     * Класс Person реализует интерфейс Comparable<Person>, который обеспечивает следующий порядок:
     * - Сортировка сначала по полю city, а затем по полю name; - Поля name, city отличны от null;
     */

    private final String name;
    private final String city;
    private final int age;


    public Person(String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }

    /**
     *  Сортировка сначала по полю city, а затем по полю name; - Поля name, city отличны от null;
     */

    @Override
    public int compareTo( Person o1) {

        if (name == null || city == null) {
            throw new NullPointerException("NULL!!!");
        }

        int result = this.city.compareToIgnoreCase(o1.city);

        if (result == 0) {
            return this.name.compareToIgnoreCase(o1.name);
        } else {
            return result;
        }
    }
}
