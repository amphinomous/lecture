package sbp.homework11_12;



import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    private final Connection connection;

    public PersonRepository(Connection connection) {
        this.connection = connection;

    }


    /**
     * Метод создания таблицы
     *
     * @throws SQLException
     */
    public void createTable() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "create table persons (" +
                "name varchar(32)," +
                "city varchar(32)," +
                "age varchar(32)" + ");";
        int affectRose = statement.executeUpdate(sql);
        System.out.println("Таблица создана" + "Количество добавленных строк: " + affectRose);
    }

    /**
     * Метод удаления таблицы
     *
     * @throws SQLException
     */
    public void dropTable() throws SQLException {
        Statement statement = connection.createStatement();
        int affectRose = statement.executeUpdate("drop table persons");
        System.out.println("Таблица удалена" + "Количество добавленных строк: " + affectRose);
    }

    /**
     * Метод добавления нового пользователя
     *
     * @param person новый пользователь
     * @throws SQLException
     */
    public void addPerson(Person person) throws SQLException {
        Statement statement = connection.createStatement();
        String sql = String.format("insert into persons ('name', 'city', 'age') VALUES ('%s', '%s', '%s')",
                person.getName(), person.getCity(), person.getAge());

        boolean hashrusultset = statement.execute(sql);
        int affectRose = statement.getUpdateCount();
        System.out.println("hashrusultset " + hashrusultset + "Пользователь добавлен" + affectRose);
    }

    /**
     * Метод изменения пользователя по имени
     *
     * @param name    Имя пользователся
     * @param newName Новое имя
     * @throws SQLException
     */
    public void changeNamePerson (String name, String newName) throws SQLException {
        Statement statement = connection.createStatement();
        int affectRose = statement.executeUpdate("update persons set name = '" + newName + "'where name ='" + name + "';");
        System.out.println("Пользователь изменен" + affectRose);
    }

    /**
     * @return Список pesron
     * @throws SQLException
     */
    public List<Person> findAll() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "select * from persons";
        List<Person> personList = new ArrayList<>();

        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {

            String name = resultSet.getString(1);
            String city = resultSet.getString(2);
            int age = resultSet.getInt(3);

            Person person = new Person(name, city, age);
            personList.add(person);

        }
        return personList;
    }

    public Person findByName(String name) throws SQLException {

        Statement statement = connection.createStatement();
        boolean hashrusultset = statement.execute("select * from persons where name = '" + name + "'");

        ResultSet resultSet = statement.getResultSet();
        if (resultSet.next() == false) {
            return null;
        }

        String nameneed = resultSet.getString("name");
        String city = resultSet.getString("city");
        int age = resultSet.getInt("age");
        System.out.println("findbyname completed name = " + name + " hashrusultset =" + hashrusultset);
        return new Person(name, city, age);

    }

    /**
     * Метод удаляет пользователя.
     * @param name
     * @param city
     * @param age
     */
    public void deletePerson(String name, String city, int age) {

        try (PreparedStatement statement = connection.prepareStatement("delete from persons where name = ? and city = ? and age = ?")) {
            statement.setString(1, name);
            statement.setString(2, city);
            statement.setInt(3, age);

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 0) {
                System.out.println("Deleted lines " + affectedRows);
            } else {
                System.out.println("No such person!!!");
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Проверка существует ли пользователь
     * @param person
     * @return true если существует
     */
    public boolean personExist (Person person) {

        try (PreparedStatement statement = connection.prepareStatement("select * from PersonDB where name = ? and city = ? and age = ?;")) {
            statement.setString(1, person.getName());
            statement.setString(2, person.getCity());
            statement.setInt(3, person.getAge());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;

            } else {
                return false;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }
}

