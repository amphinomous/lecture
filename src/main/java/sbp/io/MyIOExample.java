package sbp.io;


import java.io.*;
import java.util.Date;


public class MyIOExample {
    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {
        File myfile = new File(fileName);

        if (!myfile.exists()) return false;

        System.out.println("Абсолютный путь: " + myfile.getAbsolutePath());
        System.out.println("Родительский путь: " + myfile.getParent());

        if (myfile.isDirectory()) return false;

        System.out.println("Размер : " + myfile.length() + " байт.");
        System.out.println("Время последнего изменения: " + new Date(myfile.lastModified()));

        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileInputStream} и {@link FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException {

        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);

            int bytesReader = inputStream.read();
            while (bytesReader != -1) {
                outputStream.write(bytesReader);
                bytesReader = inputStream.read();
            }
        } catch (IOException e) {
            System.out.println("IOException.");
            e.printStackTrace();
            return false;
        } finally {
            if (inputStream != null && outputStream != null) {
                inputStream.close();
                outputStream.close();
            }
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link BufferedInputStream} и {@link BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

        BufferedInputStream fis = new BufferedInputStream(new FileInputStream(sourceFileName));
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(destinationFileName));

        byte[] buff = new byte[4096];
        int bytes = fis.read(buff);
        while (bytes != -1) {
            fos.write(buff, 0, bytes);
            bytes = fis.read(buff);
        }
        fos.flush();
        if (sourceFileName.length() == destinationFileName.length()) {
            System.out.println("Файл скопирован");
            return true;

        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileReader} и {@link FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) {
        int a;
        System.out.println("Начинается копирование файла");
        try {

            FileWriter fout = new FileWriter(sourceFileName);

            FileReader fin = new FileReader(destinationFileName);

            a = fin.read();

            while (a != -1) {

                if (a == (int) ' ') a = (int) '_';
                fout.write((char) a);
                a = fin.read();
            }

            fout.close();
            fin.close();

        } catch (IOException e) {
            System.out.println("Ошибка ввода/вывода: " + e);
        }

        return sourceFileName.length() == destinationFileName.length();

    }
}