package sbp.homework9_10;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindDuplicatesInStream1 {


    /**
     * Поиск через hashset
     * @param collection
     * @param <T>
     * @return
     */
    public static <T> Set<T> findDuplicates1(Collection<T> collection) {
        Set<T> elements = new HashSet<>();
        return collection.stream()
                .filter(e -> !elements.add(e))
                .collect(Collectors.toSet());
    }

    /**
     * Поиск с помощью Collectors.groupingBy
     * @param collection
     * @param <T>
     * @return
     */
    public static <T> Set<T> findDuplicates2(Collection<T> collection) {
        return collection.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    /**
     * Поиск с помощью утилитного Collections.frequency
     * @param collection
     * @param <T>
     * @return
     */
    public static <T> Set<T> findDuplicates3(Collection<T> collection) {
        return collection.stream()
                .filter(e -> Collections.frequency(collection, e) > 1)
                .collect(Collectors.toSet());
    }
}

