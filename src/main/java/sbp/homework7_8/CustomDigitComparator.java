package sbp.homework7_8;

import java.util.Comparator;

/**
 //Написать компаратор CustomDigitComparator, который реализует интерфейс Comparator<Integer>.
 //Класс CustomDigitComparator определяет следующий порядок: Сначала четные числа, затем нечетные
 //На вход подаются числа, отличные от null o1 o2;
 //Сначала четные числа, затем нечетные

 //Проверяем что бы не было нулей
 //Если равны возвращаем 0
 //Если Оба четные возвращаем -1
 //Во всех остальных случаях (число нечетное) возвращаем 1
 */



public class CustomDigitComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        if ((o1 == null || o2 == null)) {
            try {
                throw new Throwable("null!!!");
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
            if ((o1 % 2) == (o2 % 2)) return 0;

            if (o1 % 2 == 0) {
                return -1;
            }
            return 1;

    }
}









